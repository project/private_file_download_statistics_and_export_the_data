# Private file downloaded users report statistics and export the reports

INTRODUCTION
------------
With the help of this module, we can track the user details who ever downloaded 
the private files, and this module has the option to view, Delete & export the 
user data to a CSV file along with the user latetst downloaded time.
BENEFITS
--------
 1) Configurable Private File Fields
	This module allows site administrators to configure the private file 
	field name in the back end, which helps track user details whenever 
	users download the file from the same field.

 2) Integrated csv export ability
    The site administrators can use this configuration, to export only the 
	required user field information from this file statistics.
	
 NOTE: Downloads are NOT counted for the administrative user (user 1).

REQUIREMENTS
------------
Drupal 8.x, 9.x, 10.x

To enable downloads tracking, files must be stored in the private file system.
Follow this steps to setup 'Creating a Drupal 8 Private File System'.

INSTALLATION
------------

Install the file download user export module:
  Using composer: composer require 'drupal/private_file_download_statistics_and_export_the_data:^1.0'
  -or-
  Download it from https://www.drupal.org/project/private_file_download_statistics_and_export_the_data and 
  install it to your website.

CONFIGURATION
-------------
  a) Go to /admin/private-files-statistics/config. Here you have two 
  options where you MUST configure to Track the 
  downloads & Dashboard page.

	1) Navigate to /admin/file-statistics/config and 
		 i) Enter the private file field to track: Please enter 
		 private file field name here to track downloaded users. 
		 ex: field_private
		 ii) Enter user field names here to export: Please enter people 
		 field machine name here to export the values from users. If its
		 empty, then export link will not appear in the Dashboard.
		 ex:mail,field_firstname..etc
	 

	2) Go to /admin/files/statistics Dashboard page. Here you have 
	option to View / Delete / Export.
		  i) View : Details view of File statistics
		  ii) Delete : Delete the particular file statistics details
		  iii) Export : Export the particular statistics into CSV along 
		  with Configured users fields.
