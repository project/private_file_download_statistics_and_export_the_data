<?php

namespace Drupal\file_download_user_track_export\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * @file
 * Contains \Drupal\file_download_user_track_export\Form\FileDownloadUserTrackExportconfigForm.
 */

/**
 * Configuration form.
 */
class FileDownloadUserTrackExportconfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'file_download_user_track_export.settings_form',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'file_download_user_track_export_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('file_download_user_track_export.settings_form');

    $form['download_statistics'] = [
      '#type' => 'details',
      '#title' => t('Private file statistics configurations'),
      '#open' => TRUE,
    ];

    $form['download_statistics']['file_field_to_statistics'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Enter the private file field to track'),
      '#description' => $this->t('Please enter private file field name here to track downloaded users. (Note: Only one private field allowed here.) ex: field_private'),
      '#required' => FALSE,
      '#default_value' => $config->get('file_field_to_statistics'),
    ];

    $form['download_statistics']['file_statistics_export_user_fields'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Enter user field names here to export (If its blank then Export link will not appear in the dashboard page)'),
      '#description' => $this->t('Please enter people field machine name here to export the values from users (Note: Multiple fields allowed here with comma seperated.).ex:mail,field_firstname..'),
      '#required' => FALSE,
      '#default_value' => $config->get('file_statistics_export_user_fields'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('file_download_user_track_export.settings_form')
      ->set('file_statistics_export_user_fields', $form_state->getValue('file_statistics_export_user_fields'))
      ->set('file_field_to_statistics', $form_state->getValue('file_field_to_statistics'))
      ->save();

  }

}
