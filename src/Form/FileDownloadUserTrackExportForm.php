<?php

namespace Drupal\file_download_user_track_export\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Class FIledownloadStatisticsFilterForm.
 *
 * @package Drupal\file_download_user_track_export\Form
 */
class FileDownloadUserTrackExportForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'file_statistics_filter_form',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'filter_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $startDate = \Drupal::request()->query->get('fdate');
    $endDate = \Drupal::request()->query->get('ldate');

    $form['filters'] = [
      '#type'  => 'fieldset',
      '#title' => $this->t('Filter'),
      '#open'  => FALSE,
    ];
    $form['filters']['fdate'] = [
      '#title'         => 'From date',
      '#type'          => 'date',
      '#default_value' => $startDate,
      '#date_format'   => 'm-d-Y',
    ];
    $form['filters']['ldate'] = [
      '#title'         => 'End Date',
      '#type'          => 'date',
      '#default_value' => $endDate,
    ];
    $form['filters']['actions'] = [
      '#type'       => 'actions',
    ];
    $form['filters']['actions']['submit'] = [
      '#type'  => 'submit',
      '#value' => $this->t('Filter'),

    ];
    $form['filters']['actions']['reset'] = [
      '#type' => 'submit',
      '#value' => t('Reset'),
      '#attributes' => ['onclick' => "window.location.href='/admin/files/statistics#filter-form'"],
    ];

    return $form;

  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if ($form_state->getValue('fdate') == "") {
      $form_state->setErrorByName('fdate', $this->t('You must enter a valid date'));
    }
    elseif ($form_state->getValue('ldate') == "") {
      $form_state->setErrorByName('ldate', $this->t('You must enter a valid end date'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $field = $form_state->getValues();
    $fname = $field["fdate"];
    $marks = $field["ldate"];
    $url = Url::fromRoute('file_download_user_track_export.download_statistics_dashboard')
      ->setRouteParameters(['fdate' => $fname, 'ldate' => $marks]);
    $form_state->setRedirectUrl($url);
  }

}
