<?php

namespace Drupal\file_download_user_track_export\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Database;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Link;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * File Downlaod Statistics Controller.
 */
class FileDownloadUserTrackExportController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * Getting the results for the dashboard of file download statistics.
   */
  public function fileStatisticsDashboard() {

    // Get parameter value while submitting filter form.
    $startDate = \Drupal::request()->query->get('fdate');
    $endDate = \Drupal::request()->query->get('ldate');

    $form['form'] = $this->formBuilder()->getForm('Drupal\file_download_user_track_export\Form\FileDownloadUserTrackExportForm');

    $header = [
      'sno' => $this->t('S.no'),
      'nodeAssoc' => $this->t('Node'),
      'fileId' => $this->t('File Id'),
      'fileName' => $this->t('File Name'),
      'user counts' => $this->t('Number of Downloads'),
      'created' => $this->t('Created'),
      'actions' => $this->t('Actions'),
    ];

    if (!empty($startDate)) {
      $function = self::getFileStatisticsLists('', $startDate, $endDate);
    }
    else {
      $function = self::getFileStatisticsLists('showAll', '', '');
    }

    $form['table'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $function,
      '#attributes' => ['class' => ['downloadstatistics']],
      '#empty' => $this->t('No records found'),
      '#attached' => [
        'library' => [
          "file_download_user_track_export/file_download_user_track_export.style_settings",
        ],
      ],
    ];

    $form['pager'] = [
      '#type' => 'pager',
    ];
    return $form;
  }

  /**
   * Get full details of the file statistics.
   */
  public function getFileStatisticsLists($opt, $startDate = '', $endDate = '') {
    $config = \Drupal::config('file_download_user_track_export.settings_form');
    $connection = Database::getConnection();
    $results = $connection->select('file_statistics_reports', 'fsr')
      ->condition('report_type', 'file_download_statistics')
      ->extend('\Drupal\Core\Database\Query\PagerSelectExtender')
      ->limit(10);
    $results->fields('fsr');

    if ($opt == 'showAll') {
      $results->orderBy('fsr.id', 'DESC');
    }
    else {
      $stDate = date_parse_from_format("Y-m-d", $startDate);
      $edDate = date_parse_from_format("Y-m-d", $endDate);

      $first_minute = mktime(0, 0, 0, $stDate['month'], $stDate['day'], $stDate['year']);
      $last_minute = mktime(23, 59, 59, $edDate['month'], $edDate['day'], $edDate['year']);
      // 1678788487 //1678423277
      $results->condition('created', [$first_minute, $last_minute], 'BETWEEN');
    }

    $resultset = $results->execute()->fetchAll(\PDO::FETCH_OBJ);

    if (count($resultset)) {
      foreach ($resultset as $k => $row) {
        $nodelinks = explode('::', $row->entityId);
        $filenode_url = Url::fromRoute('entity.node.canonical', ['node' => $nodelinks[0]]);
        $filenode = Link::fromTextAndUrl($nodelinks[1], $filenode_url);
        $fileview = Link::createFromRoute(t('View'), 'file_download_user_track_export.get_item', ['fileId' => $row->id])->toString();
        $fileexport = ($config->get('file_statistics_export_user_fields')) ? Link::createFromRoute(t('Export'), 'file_download_user_track_export.export_items', ['Id' => $row->id])->toString() : '';
        $filedelete = Link::createFromRoute(t('Delete'), 'file_download_user_track_export.delete_item', ['fileId' => $row->id])->toString();

        $operationLinks = t('@view @delete @export', [
          '@view' => $fileview,
          '@delete' => $filedelete,
          '@export' => $fileexport,
        ]);

        $rows[] = [
          ['data' => $k + 1, 'width' => '5%'],
          ['data' => $filenode, 'width' => '10%'],
          ['data' => $row->fileId , 'width' => '10%'],
          ['data' => $row->file_name, 'width' => '5%'],
          [
            'data' => count(array_filter(unserialize($row->users))) ,
            'width' => '10%',
            'style' => 'word-break:break-all;',
          ],
          ['data' => date('m-d-Y h:i:s', $row->created), 'width' => '10%'],
          ['data' => $operationLinks, 'width' => '10%'] ,
        ];

      }
      return $rows;
    }
  }

  /**
   * Statistics URL access permissions.
   */
  public function statisticsAccess(AccountInterface $account) {

    // Returns an AccountProxyInterface object and not a UserInterface object.
    $current_user = \Drupal::currentUser();
    // Numberic uid value.
    $current_user_uid = $current_user->id();
    // Array List of role IDs.
    $current_user_roles = $current_user->getRoles();

    if ($account->isAuthenticated()) {
      if (in_array('administrator', $current_user_roles)) {
        return AccessResult::allowed();
      }
    }
    return AccessResult::forbidden();
  }

  /**
   * Get details of the file downloaded statistics.
   */
  public function getFileDetails($fileId) {

    $connection = Database::getConnection();
    $query = $connection->select('file_statistics_reports', 'fsr')
      ->condition('fsr.id', $fileId, '=')
      ->fields('fsr');

    $data = $query->execute();
    $results = $data->fetchAll(\PDO::FETCH_OBJ);

    $users = array_filter(unserialize($results[0]->users));
    $usersLists = implode(",\r\n", $users);

    $rows = [
      [
        ['data' => t('File Id'), 'header' => TRUE],
        $results[0]->fileId,
      ],
      [
        ['data' => t('File Name'), 'header' => TRUE],
        $results[0]->file_name,
      ],
      [
        ['data' => t('Entity Id::Title'), 'header' => TRUE],
        $results[0]->entityId,
      ],
      [
        ['data' => t('No of downloads'), 'header' => TRUE],
        $results[0]->totoal_count,
      ],
      [
        ['data' => t('Downloaded users'), 'header' => TRUE],
        $usersLists,
      ],
      [
        ['data' => t('Created'), 'header' => TRUE],
        date('m-d-Y h:i:s', $results[0]->created),
      ],
    ];

    $build['file_statistics_table'] = [
      '#type' => 'table',
      '#rows' => $rows,
      '#attached' => [
        'library' => [
          "file_download_user_track_export/file_download_user_track_export.style_settings",
        ],
      ],
    ];

    return $build;
  }

  /**
   * Delete the details of the file downloaded statistics.
   */
  public function deleteFileStatistics($fileId) {
    if ($fileId) {
      $query = \Drupal::database()->delete('file_statistics_reports');
      $query->condition('id', $fileId);
      $query->execute();
      \Drupal::messenger()->addMessage(t("Deleted successfully."));
      $response = new RedirectResponse('/admin/files/statistics');
      $response->send();
    }
  }

  /**
   * Export the details of the file downloaded statistics.
   */
  public function exportFileStatistics() {
    $batch = [
      'title' => t('Export downloaded users lists...'),
      'operations' => [
        [
          '\Drupal\file_download_user_track_export\Controller\FileDownloadUserTrackExportController::exportdownloadedUsers',
          [\Drupal::request()->get('Id')],
        ],
      ],
      'init_message' => t('Downloaded useres export is starting.'),
      'progress_message' => t('Processed @current out of @total.'),
      'finished' => '\Drupal\file_download_user_track_export\Controller\FileDownloadUserTrackExportController::exportdownloadedUsersFinished',
      'error_message' => t('The process has encountered an error.'),
    ];

    batch_set($batch);
    return batch_process('admin/files/statistics');

  }

  /**
   * Export batch jobs $statisticsId - (param) Unique statisticsId.
   */
  public static function exportdownloadedUsers($statisticsId, &$context) {
    $userId = (\Drupal::currentUser()->id());
    $config = \Drupal::config('file_download_user_track_export.settings_form');
    $member_fields = array_filter(explode(',', $config->get('file_statistics_export_user_fields')));
    array_push($member_fields, 'downloaded_timestamp');

    if (empty($context['sandbox'])) {
      $context['sandbox']['progress'] = 0;
    }

    $context['results']['statisticsId'] = $statisticsId;
    $connection = Database::getConnection();
    $query = $connection->select('file_statistics_reports', 'fsr')
      ->condition('fsr.id', $statisticsId, '=')
      ->fields('fsr');
    $data = $query->execute();
    $results = $data->fetchAll(\PDO::FETCH_OBJ);
    $fileName = $results[0]->file_name;
    $nodeTitle = explode('::', $results[0]->entityId);
    $recipients = array_filter(unserialize($results[0]->users));
    $max = $context['sandbox']['max'] = count($recipients);

    $file_save_path_stream_directory = 'public://file_statistics_report_download/';
    \Drupal::service('file_system')->prepareDirectory($file_save_path_stream_directory, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS);

    $filename = 'file_statistics_report_download_' . $userId . $statisticsId . '.csv';
    unlink($filename);

    $file_path = 'public://file_statistics_report_download/' . $filename;
    // Create the file.
    $handle = fopen($file_path, 'w');
    // Write the labels to the header row.
    fputcsv($handle, $member_fields);

    $i = 0;
    foreach ($recipients as $index => $data) {
      $context['sandbox']['progress']++;
      $userArr = user_load_by_mail($data);

      if ($userArr) {
        if ($userArr->id() != 1) {
          $userData = [];
          foreach ($member_fields as $key => $value) {
            if ($value != 'downloaded_timestamp') {
              array_push($userData, $userArr->get($value)->value);
            }
          }
          array_push($userData, date('m-d-Y g:i A', $index));
          if (!empty(array_filter($userData))) {
            fputcsv($handle, $userData);
          }

          if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
            $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
          }
          $i++;
        }
      }
      else {
        $context['results']["failed_records_data"][] = $data;
      }
    }
    fclose($handle);

    // Show message updating user on how many subscribers have been exported.
    $context['message'] = t('Exported @max of @total Users.', [
      '@max' => $i,
      '@total' => $max,
    ]);
  }

  /**
   * Exported users batch finished callback.
   */
  public static function exportdownloadedUsersFinished($success, $results, $operations) {

    if ($success) {
      $userId = (\Drupal::currentUser()->id());
      $statisticsId = $results['statisticsId'];
      $link = "/export/csv/" . $statisticsId;
      // $output = '<a href=' . $link . '>Click here</a> to download';
      \Drupal::messenger()->addMessage(t("Members list exported successfully and <a href='@link_output'>Click here</a> to download", ['@link_output' => $link]));
      if ($results['failed_records_data']) {
        \Drupal::logger('Export Download reports Failed Records')->notice('<pre>Failed Emails: <code>' . print_r($results['failed_records_data'], TRUE) . '</code></pre>');
      }
    }
    else {
      $error_operation = reset($operations);
      \Drupal::messenger()->addMessage(t('Error'));
    }

  }

  /**
   * Download the exported csv file.
   */
  public function fileStatisticsExportCsv($uid = '') {
    $userId = (\Drupal::currentUser()->id());
    $filename = 'file_statistics_report_download_' . $userId . $uid . '.csv';
    $uri_prefix = 'public://file_statistics_report_download/';
    $uri = $uri_prefix . $filename;
    if (file_exists($uri)) {
      $headers = [
        'Content-Type' => 'text/csvtext/csv; utf-8',
        'Content-Description' => 'File Download',
        'Content-Disposition' => 'attachment; filename=' . $filename,
      ];
    }
    else {
      \Drupal::messenger()->addMessage(t("No files to download for this user, Please export the statistics and try again."));
      $response = new RedirectResponse("/admin/files/statistics");
      $response->send();
    }

    return new BinaryFileResponse($uri, 200, $headers, TRUE);
  }

  /**
   * Fetch field machine names.
   */
  public static function getFieldMachineNames() {
    $result = \Drupal::service("entity_field.manager")->getFieldMap()["user"];
    foreach ($result as $key => $val) {
      if ($result[$key]['type'] == "string" || $key == "mail") {
        $fieldTokens[] = $key;
      }
    }
    return $fieldTokens;
  }

}
